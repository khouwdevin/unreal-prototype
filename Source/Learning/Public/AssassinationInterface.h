#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "AssassinationInterface.generated.h"

UINTERFACE(MinimalAPI)
class UAssassinationInterface : public UInterface
{
	GENERATED_BODY()
};

class LEARNING_API IAssassinationInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AssassinationBack(FRotator& rotator, FVector& position);
};
