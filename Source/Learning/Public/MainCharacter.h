#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameModeBase.h"
#include "AssassinationInterface.h"
#include "C:/Program Files/Epic Games/UE_5.3/Engine/Plugins/Animation/MotionWarping/Source/MotionWarping/Public/MotionWarpingComponent.h"

#include "MainCharacter.generated.h"

UCLASS(config=Game)
class LEARNING_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMainCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	UCameraComponent* mainCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	USpringArmComponent* cameraBoom;

	// Inputs
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* LookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* RunAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* WalkAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* CrouchAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* ADSAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputAction* AssassinationAction;

	// Motion Warping
	UPROPERTY(EditAnywhere, Category = Variable)
	UMotionWarpingComponent* motionwarping;

	// Variables
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = Variable)
	float runmultiplier = 2.f;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadOnly, Category = Variable)
	float walkmultiplier = 0.5f;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = Variable)
	bool isADS = false;

	bool isCrouch = false;

protected:
	UPROPERTY(Replicated, Editanywhere, BlueprintReadOnly, Category = Variable)
	float rundecreaser = 10.f;

	UPROPERTY(Replicated, BlueprintReadOnly)
	float stamina = 100.f;

	UPROPERTY(Replicated, BlueprintReadOnly)
	float health = 100.f;

	bool isRunning = false;
	bool canAssassinate = false;

	UPROPERTY(BlueprintReadWrite)
	bool inJumping = false;

	UPROPERTY(BlueprintReadWrite)
	bool isDoAction = false;

	float nostaminarunspeed;
	float normalspeed;
	float runspeed;
	float walkspeed;

	const float heightmeasurement[3] = {0, 70.f, 140.f};
	const float fowardmeasurement[3] = { 120.f, 90.f, 180.f };

protected:
	virtual void BeginPlay() override;

	void Move(const FInputActionValue& value);

	void Look(const FInputActionValue& value);

	virtual void Jump() override;

	UFUNCTION(BlueprintImplementableEvent)
	void ExecVault(FVector posstart, FVector posmiddle, FVector posland);

	UFUNCTION(BlueprintImplementableEvent)
	void ExecHangTo(FVector hangpos, FRotator rotation);

	UFUNCTION(BlueprintImplementableEvent)
	void ExecWallRun(FVector firststeploc, FVector topledge, FVector earlyrun, FRotator rotation);

	UFUNCTION(BlueprintImplementableEvent)
	void ExecAssassination(AActor* actor);

	UFUNCTION(Server, Unreliable)
	void RunOnServer(bool status);
	void RunOnServer_Implementation(bool status);

	void Run(bool status);

	UFUNCTION(Server, Unreliable)
	void WalkOnServer(bool status);
	void WalkOnServer_Implementation(bool status);

	void Walk(bool status);

	void Crouch();

	UFUNCTION(Server, Unreliable)
	void ADSOnServer(bool status);
	void ADSOnServer_Implementation(bool status);

	void ADS(bool status);

	void Assassination();

	void Vault(FVector& vaultstart);
	
	void HangTo(FVector& hangpos, FVector& impactnormal);
	void JumpHangTo(FVector& hangpos, FVector& impactnormal);

	void WallRun(FVector& firststeploc, FVector& impactnormal);

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable)
	void SpawnPlayer();

	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return cameraBoom; }
	FORCEINLINE UCameraComponent* GetMainCamera() const { return mainCamera; }
};
