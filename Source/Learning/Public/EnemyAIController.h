// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AiPerceptionComponent.h"
#include <Perception/AISense_Sight.h>
#include "BehaviorTree/BlackboardComponent.h"

#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class LEARNING_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()

public:
	AEnemyAIController();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Variable)
	UBehaviorTree* btree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Variable)
	UBlackboardData* blackboard_asset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Variable)
	UAIPerceptionComponent* AIperception;

protected:
	UBlackboardComponent* blackboard_component;
	
public:
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	void OnTargetPerceptionUpdated(AActor* actor, FAIStimulus stimulus);
};
