#include "EnemyAIController.h"

AEnemyAIController::AEnemyAIController()
{
	blackboard_component = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard"));
	blackboard_component->InitializeBlackboard(*blackboard_asset);
}

void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();

	RunBehaviorTree(btree);
}

void AEnemyAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemyAIController::OnTargetPerceptionUpdated(AActor* actor, FAIStimulus stimulus)
{
	if (stimulus.Type == UAISense::GetSenseID<UAISense_Sight>() && stimulus.WasSuccessfullySensed())
	{
		blackboard_component->SetValueAsObject(TEXT("TargetActor"), actor);
	}
	else
	{
		blackboard_component->SetValueAsObject(TEXT("TargetActor"), nullptr);
	}
}

