#include "MainCharacter.h"


AMainCharacter::AMainCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0, 500.f, 0);

	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 250.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;
	GetCharacterMovement()->MaxWalkSpeedCrouched = 250.f;

	SetReplicateMovement(true);
	SetReplicates(true);

	bReplicates = true;

	cameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	cameraBoom->SetupAttachment(RootComponent);
	cameraBoom->TargetArmLength = 400.f;
	cameraBoom->bUsePawnControlRotation = true;

	mainCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
	mainCamera->SetupAttachment(cameraBoom, USpringArmComponent::SocketName);
	mainCamera->bUsePawnControlRotation = false;
}

void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	normalspeed = 250.f;
	nostaminarunspeed = normalspeed * 1.4f;
	runspeed = normalspeed * runmultiplier;
	walkspeed = normalspeed * walkmultiplier;
	
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!GetCharacterMovement()->IsFalling()) {
		inJumping = false;
	}

	if (isRunning) 
	{
		if (stamina > 0)
			stamina -= rundecreaser * DeltaTime;		
		else 
			GetCharacterMovement()->MaxWalkSpeed = nostaminarunspeed;
	}
	else if (!isRunning && stamina <= 100.f)
	{
		if (stamina > 100.f) stamina = 100.f;
		else stamina += rundecreaser * DeltaTime;
	}

	if (inJumping) {
		if (isDoAction)
			return;

		const FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("Trace")), true, this);

		FHitResult hit;

		FVector actorLoc = GetActorLocation() - FVector(0, 0, 30.f);
		FVector actorfoward = GetActorForwardVector() * 90.f;

		FVector fowardtemp = actorfoward + FVector(actorLoc.X, actorLoc.Y, actorLoc.Z + (heightmeasurement[2]));
		FVector actorloctemp = FVector(actorLoc.X, actorLoc.Y, actorLoc.Z + (heightmeasurement[2]));

		bool isHit = GetWorld()->LineTraceSingleByChannel(hit, actorloctemp, fowardtemp, ECC_Visibility, traceParams);

		if (isHit)
		{
			FVector center = (fowardtemp - actorloctemp) / 2 + actorloctemp;
			FVector rotation = FVector(0, 90.f, GetActorRotation().Euler().Z);

			FVector centertemp = (fowardtemp - hit.Location) / 2 + hit.Location;

			float length = FVector::Distance(hit.Location, fowardtemp) / 2;

			DrawDebugCapsule(GetWorld(), center, 20.f, 6.f, FQuat::MakeFromEuler(rotation), FColor::Orange, false, 2.f);
			DrawDebugCapsule(GetWorld(), centertemp, length, 6.f, FQuat::MakeFromEuler(rotation), FColor::Green, false, 2.f);

			JumpHangTo(hit.Location, hit.ImpactNormal);
		}
	}
}

void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {

		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &AMainCharacter::Jump);

		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AMainCharacter::Move);

		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AMainCharacter::Look);

		EnhancedInputComponent->BindAction(RunAction, ETriggerEvent::Started, this, &AMainCharacter::Run, true);
		EnhancedInputComponent->BindAction(RunAction, ETriggerEvent::Completed, this, &AMainCharacter::Run, false);

		EnhancedInputComponent->BindAction(WalkAction, ETriggerEvent::Started, this, &AMainCharacter::Walk, true);
		EnhancedInputComponent->BindAction(WalkAction, ETriggerEvent::Completed, this, &AMainCharacter::Walk, false);

		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Triggered, this, &AMainCharacter::Crouch);

		EnhancedInputComponent->BindAction(ADSAction, ETriggerEvent::Started, this, &AMainCharacter::ADS, true);
		EnhancedInputComponent->BindAction(ADSAction, ETriggerEvent::Completed, this, &AMainCharacter::ADS, false);

		EnhancedInputComponent->BindAction(AssassinationAction, ETriggerEvent::Started, this, &AMainCharacter::Assassination);
	}
}

void AMainCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const 
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMainCharacter, stamina)
	DOREPLIFETIME(AMainCharacter, health)
}

void AMainCharacter::SpawnPlayer()
{
	// AActor* const spawnpoint = AGameModeBase::FindPlayerStart(this->Controller, "Player1");
}

void AMainCharacter::Move(const FInputActionValue& value)
{
	FVector2D MovementVector = value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void AMainCharacter::Jump()
{
	if (inJumping || isDoAction)
		return;

	inJumping = true;

	const FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("Trace")), true, this);

	FHitResult hit;

	FVector actorLoc = GetActorLocation() - FVector(0, 0, 30.f);

	bool isHit = false;
	bool isMiddleHit = false;

	int index = 0;

	for (int i = 2; i > -1; i--)
	{
		float halfHeight = fowardmeasurement[i] / 2;

		FVector fowardtemp = (GetActorForwardVector() * fowardmeasurement[i]) + FVector(actorLoc.X, actorLoc.Y, actorLoc.Z + (heightmeasurement[i]));
		FVector actorloctemp = FVector(actorLoc.X, actorLoc.Y, actorLoc.Z + (heightmeasurement[i]));

		isHit = GetWorld()->LineTraceSingleByChannel(hit, actorloctemp, fowardtemp, ECC_Visibility, traceParams);

		if (isHit)
		{
			FVector center = (fowardtemp - actorloctemp) / 2 + actorloctemp;
			FVector rotation = FVector(0, 90.f, GetActorRotation().Euler().Z);

			FVector centertemp = (fowardtemp - hit.Location) / 2 + hit.Location;

			float length = FVector::Distance(hit.Location, fowardtemp) / 2;

			DrawDebugCapsule(GetWorld(), center, halfHeight, 6.f, FQuat::MakeFromEuler(rotation), FColor::Orange, false, 2.f);
			DrawDebugCapsule(GetWorld(), centertemp, length, 6.f, FQuat::MakeFromEuler(rotation), FColor::Green, false, 2.f);

			index = i;

			break;
		}
	}

	if (isHit)
	{
		if (index == 0)
		{
			Vault(hit.Location);
		}
		else if (index == 1)
		{
			HangTo(hit.Location, hit.ImpactNormal);
		}
		else if (index == 2)
		{
			float dist = FVector::Dist2D(hit.Location, GetActorLocation());

			if (dist < 90.f) {
				if (isCrouch)
				{
					isCrouch = false;

					ACharacter::UnCrouch(isCrouch);
				}

				ACharacter::Jump();

				return;
			}

			WallRun(hit.Location, hit.ImpactNormal);
		}
	}
	else
	{
		if (isCrouch)
		{
			isCrouch = false;

			ACharacter::UnCrouch(isCrouch);
		}

		ACharacter::Jump();
	}
}

void AMainCharacter::Look(const FInputActionValue& value)
{
	FVector2D LookAxisVector = value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AMainCharacter::RunOnServer_Implementation(bool status)
{
	isRunning = status;

	if (status)
	{
		GetCharacterMovement()->MaxWalkSpeed = runspeed;
		isCrouch = false;
		ACharacter::UnCrouch(isCrouch);
	}
	else
		GetCharacterMovement()->MaxWalkSpeed = normalspeed;
}

void AMainCharacter::Run(bool status)
{
	RunOnServer(status);

	isRunning = status;

	if (status)
	{
		GetCharacterMovement()->MaxWalkSpeed = runspeed;
		isCrouch = false;
		ACharacter::UnCrouch(isCrouch);
	}
	else
		GetCharacterMovement()->MaxWalkSpeed = normalspeed;
}

void AMainCharacter::WalkOnServer_Implementation(bool status)
{
	if (status)
		GetCharacterMovement()->MaxWalkSpeed = walkspeed;
	else
		GetCharacterMovement()->MaxWalkSpeed = normalspeed;
}

void AMainCharacter::Walk(bool status)
{
	WalkOnServer(status);

	if (status)
		GetCharacterMovement()->MaxWalkSpeed = walkspeed;
	else
		GetCharacterMovement()->MaxWalkSpeed = normalspeed;
}

void AMainCharacter::Crouch()
{
	isCrouch = !isCrouch;

	isRunning = false;

	if (isCrouch)
		ACharacter::Crouch(isCrouch);
	else
		ACharacter::UnCrouch(isCrouch);
}

void AMainCharacter::ADS(bool status)
{
	ADSOnServer(status);

	isADS = status;
}

void AMainCharacter::ADSOnServer_Implementation(bool status)
{
	isADS = status;
}

void AMainCharacter::Assassination()
{
	TArray<AActor*> actors;

	GetOverlappingActors(actors);

	for (auto& actor : actors)
	{
		if (actor->GetClass()->ImplementsInterface(UAssassinationInterface::StaticClass()))
		{
			ExecAssassination(actor);

			break;
		}
	}
}

void AMainCharacter::Vault(FVector& vaultstart)
{
	bool isVault = false;
	bool isMiddleHit = false;
	bool isHit = false;

	const FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("Trace")), true, this);

	FHitResult hit;

	FVector vaultmiddle, vaultland;

	for (int i = 0; i < 3; i++)
	{
		FVector uppoint = (GetActorForwardVector() * (i * 80.f)) + vaultstart + FVector(0, 0, 70.f);
		FVector bottompoint = uppoint - FVector(0, 0, 90.f);

		isHit = GetWorld()->LineTraceSingleByChannel(hit, uppoint, bottompoint, ECC_Visibility, traceParams);

		if (isHit)
		{
			FVector center = FVector(uppoint.X, uppoint.Y, uppoint.Z - 35.f);
			FVector centertemp = (hit.Location - bottompoint) / 2 + bottompoint;

			float length = FVector::Distance(hit.Location, bottompoint) / 2;

			DrawDebugCapsule(GetWorld(), center, 35.f, 6.f, FQuat::Identity, FColor::Orange, false, 2.f);
			DrawDebugCapsule(GetWorld(), centertemp, length, 6.f, FQuat::Identity, FColor::Green, false, 2.f);

			vaultmiddle = hit.Location;

			isMiddleHit = true;
		}
		else
		{
			bottompoint = uppoint - FVector(0, 0, 200.f);

			isHit = GetWorld()->LineTraceSingleByChannel(hit, uppoint, bottompoint, ECC_Visibility, traceParams);
			DrawDebugLine(GetWorld(), uppoint, bottompoint, FColor::Blue, false, 2.f);

			if (isHit)
			{
				vaultland = hit.Location;
				isVault = true;
				break;
			}
		}
	}

	if (isVault && isMiddleHit)
	{
		isDoAction = true;

		ExecVault(vaultstart, vaultmiddle, vaultland);
	}
	else
	{
		if (isCrouch)
		{
			isCrouch = false;

			ACharacter::UnCrouch(isCrouch);
		}

		ACharacter::Jump();
	}
}

void AMainCharacter::HangTo(FVector& hangpos, FVector& impactnormal)
{
	const FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("Trace")), true, this);
	FVector startpos = hangpos + (GetActorForwardVector() * 2.f) + FVector(0, 0, 80.f);
	FVector endpos = startpos + FVector(0, 0, -160.f);

	FHitResult hit;
	bool isHit = false;

	isHit = GetWorld()->LineTraceSingleByChannel(hit, startpos, endpos, ECC_Visibility, traceParams);

	if (isHit)
	{
		float length = FVector::Distance(hit.Location, endpos);
		FVector center = endpos + FVector(0, 0, length / 2);
		FVector centerstart = hangpos + (GetActorForwardVector() * 2.f);

		FRotator impactrotation = FRotationMatrix::MakeFromX(impactnormal).Rotator();
		FRotator rotationtowall = FRotator(GetActorRotation().Pitch, impactrotation.Yaw + 180.f, GetActorRotation().Roll);
	
		DrawDebugCapsule(GetWorld(), centerstart, 80.f, 6.f, FQuat::Identity, FColor::Orange, false, 2.f);
		DrawDebugCapsule(GetWorld(), center, length / 2, 6.f, FQuat::Identity, FColor::Green, false, 2.f);

		isDoAction = true;

		ExecHangTo(hit.Location, rotationtowall);
	}
	else
	{
		if (isCrouch)
		{
			isCrouch = false;

			ACharacter::UnCrouch(isCrouch);
		}

		ACharacter::Jump();
	}
}

void AMainCharacter::JumpHangTo(FVector& hangpos, FVector& impactnormal)
{
	const FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("Trace")), true, this);
	FVector startpos = hangpos + (GetActorForwardVector() * 2.f) + FVector(0, 0, 10.f);
	FVector endpos = startpos + FVector(0, 0, -20.f);

	FHitResult hit;
	bool isHit = false;

	isHit = GetWorld()->LineTraceSingleByChannel(hit, startpos, endpos, ECC_Visibility, traceParams);

	if (isHit)
	{
		float length = FVector::Distance(hit.Location, endpos);
		FVector center = endpos + FVector(0, 0, length / 2);
		FVector centerstart = hangpos + (GetActorForwardVector() * 2.f);

		FRotator impactrotation = FRotationMatrix::MakeFromX(impactnormal).Rotator();
		FRotator rotationtowall = FRotator(GetActorRotation().Pitch, impactrotation.Yaw + 180.f, GetActorRotation().Roll);

		DrawDebugCapsule(GetWorld(), centerstart, 10.f, 6.f, FQuat::Identity, FColor::Orange, false, 2.f);
		DrawDebugCapsule(GetWorld(), center, length / 2, 6.f, FQuat::Identity, FColor::Green, false, 2.f);

		isDoAction = true;

		ExecHangTo(hit.Location, rotationtowall);
	}
	else
	{
		if (isCrouch)
		{
			isCrouch = false;

			ACharacter::UnCrouch(isCrouch);
		}

		ACharacter::Jump();
	}
}

void AMainCharacter::WallRun(FVector& firststeploc, FVector& impactnormal)
{
	const FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("Trace")), true, this);
	
	FVector endpos = firststeploc + (GetActorForwardVector() * 30.f);
	FVector startpos = endpos + FVector(0, 0, 100.f);

	FHitResult hit;
	bool isHit = false;

	FVector steploc = FVector(firststeploc.X, firststeploc.Y, GetActorLocation().Z + 40.f) - (GetActorForwardVector() * 30.f);

	isHit = GetWorld()->LineTraceSingleByChannel(hit, startpos, endpos, ECC_Visibility, traceParams);

	if (isHit)
	{
		float length = FVector::Distance(hit.Location, endpos);
		FVector center = endpos + FVector(0, 0, length / 2);
		FVector centerstart = firststeploc + (GetActorForwardVector() * 2.f);

		FRotator impactrotation = FRotationMatrix::MakeFromX(impactnormal).Rotator();
		FRotator rotationtowall = FRotator(GetActorRotation().Pitch, impactrotation.Yaw + 180.f, GetActorRotation().Roll);

		DrawDebugCapsule(GetWorld(), centerstart, 80.f, 6.f, FQuat::Identity, FColor::Orange, false, 2.f);
		DrawDebugCapsule(GetWorld(), center, length / 2, 6.f, FQuat::Identity, FColor::Green, false, 2.f);

		isDoAction = true;

		ExecWallRun(steploc, hit.Location, GetActorLocation(), rotationtowall);
	}
	else
	{
		if (isCrouch)
		{
			isCrouch = false;

			ACharacter::UnCrouch(isCrouch);
		}

		ACharacter::Jump();
	}
}