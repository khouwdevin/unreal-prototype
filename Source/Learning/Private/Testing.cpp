#include "Testing.h"

ATesting::ATesting()
{
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATesting::BeginPlay()
{
	Super::BeginPlay();
	
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

// Called every frame
void ATesting::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATesting::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(VaultAction, ETriggerEvent::Started, this, &ATesting::Vault);
	}
}

void ATesting::Vault()
{
	TArray<FHitResult> hits;
	FHitResult hit;
	const float multiplier = 30.f;

	const FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("Trace")), true, this);

	// const bool isHit = GetWorld()->SweepMultiByChannel(hits, GetActorLocation(), GetActorForwardVector() * multiplier, FQuat::Identity, ECC_Visibility, FCollisionShape::MakeSphere(12.f), traceParams);
	GetWorld()->SweepSingleByChannel(hit, GetActorLocation(), GetActorForwardVector() * multiplier, FQuat::Identity, ECC_Visibility, FCollisionShape::MakeSphere(12.f), traceParams);
	GetWorld()->LineTraceSingleByChannel(hit, GetActorLocation(), GetActorForwardVector() * multiplier, ECC_Visibility, traceParams);

	// DrawDebugLine(GetWorld(), GetActorLocation(), GetActorForwardVector() * multiplier, FColor::Orange, false, 2.f);

	UE_LOG(LogTemp, Warning, TEXT("Test"));
}
